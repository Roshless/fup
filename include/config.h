#ifndef _FUP_CONFIG_H
#define _FUP_CONFIG_H

struct settings {
	char *server;
	char *username;
	char *password;
};

int load_config(struct settings *set);
void destroy_config(struct settings *set);

#endif
