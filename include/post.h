#ifndef _FUP_POST_H
#define _FUP_POST_H
#include <stdlib.h>

#include "config.h"
#include "options.h"

struct memory {
	char *response;
	size_t size;
};

char *post_paste(struct options *opr, struct settings *set);
char *post_named_paste(struct options *opr, struct settings *set);
char *delete_paste(struct options *opr, struct settings *set);
char *list_user_pastes(struct options *opr, struct settings *set);
char *list_anonymous_pastes(struct options *opr, struct settings *set);

#endif
