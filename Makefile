BINARY = fup
LIBS = -lcurl
CFLAGS = -g -Wall -Wextra -Werror -Wno-unused-parameter
INCLUDE += -Iinclude
BUILDDIR = .build

BINDIR += $(DESTDIR)$(PREFIX)/bin

.PHONY: default all clean install

default: all
all: $(BINARY)

OBJECTS = $(patsubst src/%.c, $(BUILDDIR)/%.o, $(wildcard src/*.c))

$(BUILDDIR)/%.o: src/%.c
	@mkdir -p $(BUILDDIR)
	$(CC) -c -o $@ $(CFLAGS) $(INCLUDE) $<

$(BINARY): $(OBJECTS)
	$(CC) $^ -Wall $(LIBS) -o $@

install: $(TARGET)
	mkdir -p $(BINDIR)
	install -m755 $(BINARY) $(BINDIR)/$(BINARY)

clean:
	rm -rf $(BUILDDIR) $(BINARY)
