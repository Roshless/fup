#include <stdlib.h>

#include "options.h"

void init_options(struct options *opt)
{
	opt->operation_code = NORMAL;
	opt->paste_content = NULL;
	opt->paste_size = 0;
}

void destroy_options(struct options *opt) { free(opt->paste_content); }
